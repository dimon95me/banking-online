package com.example.bankingonline.kabinet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bankingonline.R;
import com.example.bankingonline.deposit.DepositActivity;
import com.example.bankingonline.kabinet.klient_update.KlientUpdateActivity;
import com.example.bankingonline.kredyt.KredytActivity;
import com.example.bankingonline.model.Klient;
import com.example.bankingonline.score.ScoreActivity;

public class KabinetActivity extends AppCompatActivity implements KabinetView{

    protected int id_klient;
    protected Klient klient;

    Button actionDeposit, actionScore, actionKredyt;
    TextView nameTextView, lastNameTextView, addressTextView, phoneTextView, emailTextView;
    ProgressDialog progressDialog;

    KabinetPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kabinet);

        id_klient = getIntent().getIntExtra("id_klient", -1);

        actionDeposit = findViewById(R.id.kabinet_action_deposit);
        actionScore = findViewById(R.id.kabinet_action_score);
        actionKredyt = findViewById(R.id.kabinet_action_kredyt);
        nameTextView = findViewById(R.id.kabinet_name);
        lastNameTextView = findViewById(R.id.kabinet_last_name);
        addressTextView = findViewById(R.id.kabinet_address);
        phoneTextView = findViewById(R.id.kabinet_phone);
        emailTextView = findViewById(R.id.kabinet_email);

        presenter = new KabinetPresenter(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        presenter.getKlientData(id_klient);


        actionScore.setOnClickListener(view -> {
            Intent intent = new Intent(this, ScoreActivity.class);
            intent.putExtra("id_klient", id_klient);
            startActivity(intent);
        });

        actionDeposit.setOnClickListener(view -> {
            Intent intent = new Intent(this, DepositActivity.class);
            intent.putExtra("id_klient", id_klient);
            startActivity(intent);
        });

        actionKredyt.setOnClickListener(view -> {
            Intent intent = new Intent(this, KredytActivity.class);
            intent.putExtra("id_klient", id_klient);
            startActivity(intent);
        });
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void onRequestSuccess(Klient klient) {
        this.klient = klient;

        nameTextView.setText(klient.getName());
        lastNameTextView.setText(klient.getLast_name());
        addressTextView.setText(klient.getAddress());
        phoneTextView.setText(klient.getPhone());
        emailTextView.setText(klient.getEmail());
    }

    @Override
    public void onRequestError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.update_klient_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_klient_update:
                //touch function
                Intent intent = new Intent(this, KlientUpdateActivity.class);
                    if (klient!=null){
                        intent.putExtra("klient_obj", klient);
                    }
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
