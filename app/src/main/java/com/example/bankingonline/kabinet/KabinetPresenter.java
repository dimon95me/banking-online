package com.example.bankingonline.kabinet;

import com.example.bankingonline.api.ApiClient;
import com.example.bankingonline.api.ApiInterface;
import com.example.bankingonline.model.Klient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KabinetPresenter {

    private KabinetActivity view;

    public KabinetPresenter(KabinetActivity view) {
        this.view = view;
    }

    void getKlientData(int id_klient){
        view.showProgress();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Klient> call = apiInterface.getKlient(view.id_klient);

        call.enqueue(new Callback<Klient>() {
            @Override
            public void onResponse(Call<Klient> call, Response<Klient> response) {
                view.progressDialog.hide();

                if (response.isSuccessful()&&response.body()!=null){
                    Boolean success = response.body().getSuccess();

                    if (success){
                        view.onRequestSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<Klient> call, Throwable t) {
                view.progressDialog.hide();
                view.onRequestError(t.getLocalizedMessage());
            }
        });
    }

}
