package com.example.bankingonline.kabinet.klient_update;

import com.example.bankingonline.api.ApiClient;
import com.example.bankingonline.api.ApiInterface;
import com.example.bankingonline.model.Klient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KlientUpdatePresenter {

    private KlientUpdateView view;

    public KlientUpdatePresenter(KlientUpdateView view) {
        this.view = view;
    }

    void updateData(int idKlient, String name, String lastName, String address, String email, String phone){
        view.showProgress();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Klient> call = apiInterface.updateKlientData(idKlient, name, lastName, address, phone, email);

        call.enqueue(new Callback<Klient>() {
            @Override
            public void onResponse(Call<Klient> call, Response<Klient> response) {
                view.hideProgress();

                if (response.isSuccessful() && response.body()!=null){
                    Boolean success = response.body().getSuccess();

                    if (success){
                        view.onRequestSuccess(response.body().getMessage());
                    } else {
                        view.onRequestError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<Klient> call, Throwable t) {
                view.hideProgress();
                view.onRequestError(t.getLocalizedMessage());
            }
        });
    }
}
