package com.example.bankingonline.kabinet;

import com.example.bankingonline.model.Klient;

public interface KabinetView {

    void showProgress();
    void hideProgress();
    void onRequestSuccess(Klient klient);
    void onRequestError(String message);

}
