package com.example.bankingonline.kabinet.klient_update;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bankingonline.R;
import com.example.bankingonline.model.Klient;

public class KlientUpdateActivity extends AppCompatActivity implements KlientUpdateView{

    private Klient klient;

    EditText nameEditText, lastNameEditText, emailEditText, addressEditText, phoneEditText;
    Button changePasswordAction;

    ProgressDialog progressDialog;
    KlientUpdatePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_klient_update);

        klient =(Klient) getIntent().getSerializableExtra("klient_obj");
        presenter = new KlientUpdatePresenter(this);

        nameEditText = findViewById(R.id.klient_update_name);
        lastNameEditText = findViewById(R.id.klient_update_last_name);
        emailEditText = findViewById(R.id.klient_update_email);
        addressEditText = findViewById(R.id.klient_update_address);
        phoneEditText = findViewById(R.id.klient_update_phone);
        changePasswordAction = findViewById(R.id.klient_update_change_password_action);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        nameEditText.setText(klient.getName());
        lastNameEditText.setText(klient.getLast_name());
        emailEditText.setText(klient.getEmail());
        addressEditText.setText(klient.getEmail());
        phoneEditText.setText(klient.getPhone());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.update_klient_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_klient_update:
             //touch function
                Toast.makeText(this, "menu item is pressed", Toast.LENGTH_SHORT).show();
                presenter.updateData(klient.getId_klient(),
                        nameEditText.getText().toString(),
                        lastNameEditText.getText().toString(),
                        addressEditText.getText().toString(),
                        phoneEditText.getText().toString(),
                        emailEditText.getText().toString());
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void onRequestSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
