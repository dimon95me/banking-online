package com.example.bankingonline.kabinet.klient_update;

public interface KlientUpdateView {

    void showProgress();
    void hideProgress();
    void onRequestSuccess(String message);
    void onRequestError(String message);

}
