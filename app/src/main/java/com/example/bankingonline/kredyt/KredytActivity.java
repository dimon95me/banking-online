package com.example.bankingonline.kredyt;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.bankingonline.R;
import com.example.bankingonline.deposit.DepositView;
import com.example.bankingonline.model.Kredyt;

import java.util.List;

public class KredytActivity extends AppCompatActivity implements KredytView {

    private int idKlient;
    private RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    KredytPresenter presenter;
    KredytAdapter adapter;
    KredytAdapter.ItemClickListenerKredyt itemClickListener;

    List<Kredyt> kredyts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kredyt);

        swipeRefreshLayout = findViewById(R.id.kredyt_refresh);
        recyclerView = findViewById(R.id.kredyt_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        idKlient = getIntent().getIntExtra("id_klient", -1);

        presenter = new KredytPresenter(this);

        swipeRefreshLayout.setOnRefreshListener(()->presenter.getData(idKlient));

        itemClickListener = (((view, position) -> {
            codeForOnItemClick();
        }));
    }

    private void codeForOnItemClick() {
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetResult(List<Kredyt> kredyts) {
        adapter = new KredytAdapter(this, kredyts ,itemClickListener);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        this.kredyts = kredyts;
    }

    @Override
    public void onErrorLoading(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getData(idKlient);
    }
}
