package com.example.bankingonline.kredyt;

import com.example.bankingonline.api.ApiClient;
import com.example.bankingonline.api.ApiInterface;
import com.example.bankingonline.model.Kredyt;
import com.example.bankingonline.score.ScoreView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KredytPresenter {

    private KredytView view;

    public KredytPresenter(KredytView view) {
        this.view = view;
    }

    void getData(int id_klient){
        view.showLoading();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<Kredyt>> call = apiInterface.getKredytes(id_klient);
        call.enqueue(new Callback<List<Kredyt>>() {
            @Override
            public void onResponse(Call<List<Kredyt>> call, Response<List<Kredyt>> response) {
                view.hideLoading();
                if (response.isSuccessful() && response.body()!=null){
                    view.onGetResult(response.body());
                }else{if (response.body()==null)view.onErrorLoading("body is null");}
            }

            @Override
            public void onFailure(Call<List<Kredyt>> call, Throwable t) {
                view.hideLoading();
                view.onErrorLoading(t.getLocalizedMessage());
            }
        });
    }

}
