package com.example.bankingonline.kredyt;

import com.example.bankingonline.model.Kredyt;

import java.util.List;

public interface KredytView {

    void showLoading();
    void hideLoading();
    void onGetResult(List<Kredyt> kredyts);
    void onErrorLoading(String message);
}
