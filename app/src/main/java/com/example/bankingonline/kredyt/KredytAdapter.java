package com.example.bankingonline.kredyt;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bankingonline.R;
import com.example.bankingonline.model.Kredyt;

import java.util.List;

public class KredytAdapter extends RecyclerView.Adapter<KredytAdapter.ViewHoder> {

    private Context context;
    private List<Kredyt> kredyts;
    private ItemClickListenerKredyt itemClickListener;

    public KredytAdapter(Context context, List<Kredyt> kredyts, ItemClickListenerKredyt itemClickListener) {
        this.context = context;
        this.kredyts = kredyts;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.kredyt_list_item, viewGroup, false);
        return new ViewHoder(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder viewHoder, int i) {
        Kredyt kredyt = kredyts.get(i);
        viewHoder.nameTextView.setText("Value: "+kredyt.getName());
        viewHoder.countTextView.setText("Return left: "+String.valueOf(kredyt.getCount_left()));
    }

    @Override
    public int getItemCount() {
        return kredyts.size();
    }

    public class ViewHoder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nameTextView, countTextView;
        LinearLayout layout;
        ItemClickListenerKredyt itemClickListener;

        public ViewHoder(@NonNull View itemView, ItemClickListenerKredyt itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;

            nameTextView = itemView.findViewById(R.id.kredyt_list_item_name);
            countTextView = itemView.findViewById(R.id.kredyt_list_item_count);
            layout = itemView.findViewById(R.id.kredyt_list_item_layout);

            this.itemClickListener = itemClickListener;
            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public interface ItemClickListenerKredyt {
        void onItemClick(View view,int position);
    }
}
