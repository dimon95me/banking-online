package com.example.bankingonline.api;

import com.example.bankingonline.model.Deposit;
import com.example.bankingonline.model.Klient;
import com.example.bankingonline.model.Kredyt;
import com.example.bankingonline.model.Score;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("login.php")
    Call<Klient> loginCheck(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("klient_add.php")
    Call<Klient> registerKlient(
            @Field("name") String name,
            @Field("last_name") String lastName,
            @Field("address") String address,
            @Field("phone") String phone,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("klient_get.php")
    Call<Klient> getKlient(@Field("id_klient") int id_klient);

    @FormUrlEncoded
    @POST("scores.php")
    Call<List<Score>> getScores(@Field("id_klient") int id_klient);

    @FormUrlEncoded
    @POST("deposits.php")
    Call<List<Deposit>> getDeposits(@Field("id_klient") int id_klient);

    @FormUrlEncoded
    @POST("kredytes.php")
    Call<List<Kredyt>> getKredytes(@Field("id_klient") int id_klient);

    @FormUrlEncoded
    @POST("klient_update.php")
    Call<Klient> updateKlientData(
            @Field("id_klient") int idKlient,
            @Field("name") String name,
            @Field("last_name") String lastName,
            @Field("address") String address,
            @Field("email") String email,
            @Field("phone") String phone
    );

}
