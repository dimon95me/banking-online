package com.example.bankingonline.register;

import com.example.bankingonline.api.ApiClient;
import com.example.bankingonline.api.ApiInterface;
import com.example.bankingonline.model.Klient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter {

    private RegisterActivity view;

    public RegisterPresenter(RegisterActivity view) {
        this.view = view;
    }

    void registerKlient(String name, String lastName, String address, String phone, String email, String password){
        view.showProgress();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Klient> call = apiInterface.registerKlient(name, lastName, address, phone, email, password);

        call.enqueue(new Callback<Klient>() {
            @Override
            public void onResponse(Call<Klient> call, Response<Klient> response) {
                view.hideProgress();

                if (response.isSuccessful() && response.body()!=null){
                    Boolean success = response.body().getSuccess();

                    if (success){
                        view.onRequestSuccess(response.body().getMessage());
                    } else {
                        view.onRequestError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<Klient> call, Throwable t) {
                view.hideProgress();

                view.onRequestError(t.getLocalizedMessage());
            }
        });
    }

}
