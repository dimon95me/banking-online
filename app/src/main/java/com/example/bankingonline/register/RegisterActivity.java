package com.example.bankingonline.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bankingonline.R;
import com.example.bankingonline.login.MainActivity;

public class RegisterActivity extends AppCompatActivity implements RegisterView{

    EditText name, lastName, address, phone, email, password;
    Button register_action;
    ProgressDialog progressDialog;

    RegisterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenter(this);

        name = findViewById(R.id.register_name);
        lastName = findViewById(R.id.register_last_name);
        address = findViewById(R.id.register_address);
        phone = findViewById(R.id.register_phone);
        email = findViewById(R.id.register_email);
        password = findViewById(R.id.register_password);
        register_action = findViewById(R.id.register_action);

        register_action.setOnClickListener(view -> {
            presenter.registerKlient(name.getText().toString(),
                lastName.getText().toString(),
                address.getText().toString(),
                phone.getText().toString(),
                email.getText().toString(),
                password.getText().toString());
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void onRequestSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("email", email.getText().toString());
        startActivity(intent);
    }

    @Override
    public void onRequestError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
