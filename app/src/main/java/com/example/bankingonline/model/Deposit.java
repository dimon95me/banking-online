package com.example.bankingonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Deposit {

    @Expose
    @SerializedName("id_deposit")
    private int idDeposit;

    @Expose
    @SerializedName("start_date")
    private String startDate;

    @Expose
    @SerializedName("id_klient")
    private int idKlient;

    @Expose
    @SerializedName("count")
    private double count;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("status")
    private int status;

    @Expose
    @SerializedName("id_deposit_type")
    private int getIdDepositType;

    @Expose
    @SerializedName("success")
    private Boolean success;

    @Expose
    @SerializedName("message")
    private String message;

    public int getIdDeposit() {
        return idDeposit;
    }

    public void setIdDeposit(int idDeposit) {
        this.idDeposit = idDeposit;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getIdKlient() {
        return idKlient;
    }

    public void setIdKlient(int idKlient) {
        this.idKlient = idKlient;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getGetIdDepositType() {
        return getIdDepositType;
    }

    public void setGetIdDepositType(int getIdDepositType) {
        this.getIdDepositType = getIdDepositType;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
