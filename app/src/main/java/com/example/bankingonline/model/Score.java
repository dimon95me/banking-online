package com.example.bankingonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Score {

    @Expose
    @SerializedName("id_score")
    private int idScore;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("count")
    private double count;

    @Expose
    @SerializedName("id_klient")
    private int id_klient;

    @Expose
    @SerializedName("date_start")
    private String dateStart;

    @Expose
    @SerializedName("id_score_type")
    private int idScoreType;

    @Expose
    @SerializedName("success")
    private Boolean success;

    @Expose
    @SerializedName("message")
    private String message;

    public int getIdScore() {
        return idScore;
    }

    public void setIdScore(int idScore) {
        this.idScore = idScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public int getId_klient() {
        return id_klient;
    }

    public void setId_klient(int id_klient) {
        this.id_klient = id_klient;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public int getIdScoreType() {
        return idScoreType;
    }

    public void setIdScoreType(int idScoreType) {
        this.idScoreType = idScoreType;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
