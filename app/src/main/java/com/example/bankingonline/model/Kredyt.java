package com.example.bankingonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kredyt {

    @Expose
    @SerializedName("id_score")
    private int id_score;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("date_start")
    private String date_start;

    @Expose
    @SerializedName("count_left")
    private double count_left;

    @Expose
    @SerializedName("id_klient")
    private int id_klient;

    @Expose
    @SerializedName("id_kredyt_type")
    private int id_kredyt_type;

    @Expose
    @SerializedName("success")
    private Boolean success;

    @Expose
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId_klient() {
        return id_klient;
    }

    public void setId_klient(int id_klient) {
        this.id_klient = id_klient;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public int getId_score() {
        return id_score;
    }

    public void setId_score(int id_score) {
        this.id_score = id_score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCount_left() {
        return count_left;
    }

    public void setCount_left(double count_left) {
        this.count_left = count_left;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public int getId_kredyt_type() {
        return id_kredyt_type;
    }

    public void setId_kredyt_type(int id_kredyt_type) {
        this.id_kredyt_type = id_kredyt_type;
    }
}
