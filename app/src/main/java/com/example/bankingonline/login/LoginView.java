package com.example.bankingonline.login;

public interface LoginView {

    void showProgress();
    void hideProgress();
    void onRequestSuccess(String message, int id_klient);
    void onRequestError(String message);


}
