package com.example.bankingonline.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bankingonline.R;
import com.example.bankingonline.kabinet.KabinetActivity;
import com.example.bankingonline.register.RegisterActivity;

public class MainActivity extends AppCompatActivity implements LoginView{

    EditText login_edittext, password_edittext;
    TextView signInTextview;
    Button loginAction;
    ProgressDialog progressDialog;

    LoginPresenter presenter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login_edittext = findViewById(R.id.main_login);
        password_edittext = findViewById(R.id.main_password);
        loginAction = findViewById(R.id.main_action_login);
        signInTextview = findViewById(R.id.main_register);

        presenter = new LoginPresenter(this);


        loginAction.setOnClickListener(view -> {
            String loginText = login_edittext.getText().toString();
            String passwordText = password_edittext.getText().toString();

            presenter.loginCheck(loginText, passwordText);
        });

        signInTextview.setOnClickListener(view -> {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        if (getIntent().getStringExtra("email")!=null){
            login_edittext.setText(getIntent().getStringExtra("email"));
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void onRequestSuccess(String message, int id_klient) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        Intent intent  = new Intent(this, KabinetActivity.class);
        intent.putExtra("id_klient", id_klient);
        startActivity(intent);
    }

    @Override
    public void onRequestError(String message) {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }
}
