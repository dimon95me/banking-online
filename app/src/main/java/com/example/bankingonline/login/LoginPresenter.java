package com.example.bankingonline.login;

import android.util.Log;

import com.example.bankingonline.api.ApiClient;
import com.example.bankingonline.api.ApiInterface;
import com.example.bankingonline.model.Klient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {

    private MainActivity view;

    public LoginPresenter(MainActivity view) {
        this.view = view;
    }

    void loginCheck(String login, String password){
        view.showProgress();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Klient> call = apiInterface.loginCheck(login, password);

        call.enqueue(new Callback<Klient>() {
            @Override
            public void onResponse(Call<Klient> call, Response<Klient> response) {
                view.hideProgress();

                if (response.isSuccessful() && response.body()!=null){

                    Boolean success = response.body().getSuccess();
                    if (success){
                        view.onRequestSuccess(response.body().getMessage(), response.body().getId_klient());
                    } else{
                        view.onRequestError(response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<Klient> call, Throwable t) {
                view.hideProgress();

                view.onRequestError(t.getLocalizedMessage());
            }
        });
    }
}
