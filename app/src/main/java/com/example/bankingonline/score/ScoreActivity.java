package com.example.bankingonline.score;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.bankingonline.R;
import com.example.bankingonline.model.Score;

import java.util.List;

public class ScoreActivity extends AppCompatActivity implements ScoreView {

    protected int idKlient;
    private RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    ScorePresenter presenter;
    ScoreAdapter adapter;
    ScoreAdapter.ItemClickListenerScore itemClickListener;

    List<Score> scores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        swipeRefreshLayout = findViewById(R.id.score_refresh);
        recyclerView = findViewById(R.id.score_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


            idKlient = getIntent().getIntExtra("id_klient", -1);

        presenter = new ScorePresenter(this);

        Log.d("my_tag", "pre refreshklient: " +idKlient);
        swipeRefreshLayout.setOnRefreshListener(()->presenter.getData(idKlient));

        itemClickListener = (((view, position) -> {
            goToScoreDetails();
        }));
    }

    private void goToScoreDetails() {

    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetResult(List<Score> scores) {
        adapter = new ScoreAdapter(this, scores, itemClickListener);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        this.scores = scores;
    }

    @Override
    public void onErrorLoading(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getData(idKlient);
    }
}
