package com.example.bankingonline.score;

import android.util.Log;

import com.example.bankingonline.api.ApiClient;
import com.example.bankingonline.api.ApiInterface;
import com.example.bankingonline.model.Score;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScorePresenter {

    private ScoreView view;

    public ScorePresenter(ScoreView view) {
        this.view = view;
    }

    void getData(int idKlient) {
        view.showLoading();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<Score>> call = apiInterface.getScores(idKlient);
        call.enqueue(new Callback<List<Score>>() {
            @Override
            public void onResponse(Call<List<Score>> call, Response<List<Score>> response) {
                view.hideLoading();

                if (response.isSuccessful() && response.body()!=null){
                    view.onGetResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Score>> call, Throwable t) {
                view.hideLoading();
                view.onErrorLoading(t.getLocalizedMessage());
            }
        });
    }
}
