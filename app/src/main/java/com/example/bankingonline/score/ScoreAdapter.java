package com.example.bankingonline.score;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bankingonline.R;
import com.example.bankingonline.model.Score;

import java.util.List;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.RecyclerViewAdapter> {

    private Context context;
    private List<Score> scores;
    private ItemClickListenerScore itemClickListener;

    public ScoreAdapter(Context context, List<Score> scores, ItemClickListenerScore itemClickListener) {
        this.context = context;
        this.scores = scores;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.score_list_item, viewGroup, false);
        return new RecyclerViewAdapter(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter holder, int i) {
        Score score = scores.get(i);
        holder.nameTextView.setText(score.getName());
        holder.countTextView.setText(String.valueOf(score.getCount()));
    }

    @Override
    public int getItemCount() {
        return scores.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout layout;
        TextView nameTextView, countTextView;
        ItemClickListenerScore itemClickListener;

        public RecyclerViewAdapter(@NonNull View itemView, ItemClickListenerScore itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;

            nameTextView = itemView.findViewById(R.id.score_list_item_name);
            countTextView = itemView.findViewById(R.id.score_list_item_count);
            layout = itemView.findViewById(R.id.score_list_item_layout);

            this.itemClickListener = itemClickListener;
            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public interface ItemClickListenerScore {
        void onItemClick(View view, int position);
    }
}
