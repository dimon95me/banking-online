package com.example.bankingonline.score;

import com.example.bankingonline.model.Score;

import java.util.List;

public interface ScoreView {

    void showLoading();
    void hideLoading();
    void onGetResult(List<Score> scores);
    void onErrorLoading(String message);

}
