package com.example.bankingonline.deposit;



import android.util.Log;

import com.example.bankingonline.api.ApiClient;
import com.example.bankingonline.api.ApiInterface;
import com.example.bankingonline.model.Deposit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DepositPresenter {

    private DepositView view;

    public DepositPresenter(DepositView view) {
        this.view = view;
    }

    void getData(int idKlient){
        view.showLoading();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<Deposit>> call = apiInterface.getDeposits(idKlient);
        call.enqueue(new Callback<List<Deposit>>() {
            @Override
            public void onResponse(Call<List<Deposit>> call, Response<List<Deposit>> response) {
                view.hideLoading();

                if (response.isSuccessful() && response.body()!=null){
                    view.onGetResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Deposit>> call, Throwable t) {
                view.hideLoading();
                view.onErrorLoading(t.getLocalizedMessage());
            }
        });
    }

}
