package com.example.bankingonline.deposit;

import com.example.bankingonline.model.Deposit;
import com.example.bankingonline.model.Score;

import java.util.List;

public interface DepositView {

    void showLoading();
    void hideLoading();
    void onGetResult(List<Deposit> deposits);
    void onErrorLoading(String message);

}
