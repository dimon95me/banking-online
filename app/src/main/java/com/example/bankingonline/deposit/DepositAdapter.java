package com.example.bankingonline.deposit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bankingonline.R;
import com.example.bankingonline.model.Deposit;

import java.util.List;

public class DepositAdapter extends RecyclerView.Adapter<DepositAdapter.ViewHolder> {


    private Context context;
    private List<Deposit> deposits;
    private ItemClickListenerDeposit itemClickListener;

    public DepositAdapter(Context context, List<Deposit> deposits, ItemClickListenerDeposit itemClickListener) {
        this.context = context;
        this.deposits = deposits;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.deposit_list_item, viewGroup, false);
        return new ViewHolder(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Deposit deposit = deposits.get(i);
        viewHolder.nameTextView.setText(deposit.getName());
        viewHolder.countTextView.setText(String.valueOf(deposit.getCount()));
        viewHolder.dateTextView.setText(deposit.getStartDate());
    }

    @Override
    public int getItemCount() {
        return deposits.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView nameTextView, countTextView, dateTextView;
        ConstraintLayout layout;
        ItemClickListenerDeposit itemClickListener;

        public ViewHolder(@NonNull View itemView, ItemClickListenerDeposit itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;

            nameTextView = itemView.findViewById(R.id.deposit_list_item_name);
            countTextView = itemView.findViewById(R.id.deposit_list_item_count);
            dateTextView = itemView.findViewById(R.id.deposit_list_item_date);
            layout = itemView.findViewById(R.id.deposit_list_item_layout);

            this.itemClickListener = itemClickListener;
            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public interface ItemClickListenerDeposit {
        void onItemClick(View view, int position);
    }

}
