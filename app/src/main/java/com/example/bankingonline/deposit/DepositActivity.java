package com.example.bankingonline.deposit;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.bankingonline.R;
import com.example.bankingonline.model.Deposit;

import java.util.List;

public class DepositActivity extends AppCompatActivity implements DepositView {

    protected int idKlient;
    private RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    DepositPresenter presenter;
    DepositAdapter adapter;
    DepositAdapter.ItemClickListenerDeposit itemClickListener;

    List<Deposit> deposits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);

        swipeRefreshLayout = findViewById(R.id.deposit_refresh);
        recyclerView = findViewById(R.id.deposit_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        idKlient = getIntent().getIntExtra("id_klient", -1);

        presenter = new DepositPresenter(this);

        Log.d("my_tag", "pre refreshklient: " +idKlient);
        swipeRefreshLayout.setOnRefreshListener(()->presenter.getData(idKlient));

        itemClickListener = (((view, position) -> {
            goToDepositDetails();
        }));
    }

    private void goToDepositDetails() {

    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetResult(List<Deposit> deposits) {
        adapter = new DepositAdapter(this, deposits, itemClickListener);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        this.deposits = deposits;
    }

    @Override
    public void onErrorLoading(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getData(idKlient);
    }
}
